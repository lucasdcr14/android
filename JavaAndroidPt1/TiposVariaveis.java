package br.com.lucas;

public class TiposVariaveis {

	public static void main(String[] args) {
		
		//String - textos ex: "café", "escola", "Lucas Ribeiro"
		String minhaVariavelString = "Café";
		System.out.println(minhaVariavelString);
		
		//Variáveis números
		//byte - números ex: 1, 3, 20 de -128 até 127
		byte minhaVariavelByte = 20;
		System.out.println(minhaVariavelByte);
		
		//short - números ex: 1, 180, 900 de -32768 até 32767
		short minhaVariavelShort = 3200;
		System.out.println(minhaVariavelShort);
		
		//int - número ex: 1, 180 de -2147483648 até 2147483647
		int minhaVariavelInt = 380000;
		System.out.println(minhaVariavelInt);
		
		//long - número ex: 1, 500000 de -9223372036854775808 até 9223372036854775807
		long minhaVariavelLong = 1000000000;
		System.out.println(minhaVariavelLong);
		
		//float - números decimais ex: 1.2535 / precisao de 7
		float precoFloat = 2.50f;
		System.out.println(precoFloat);
		
		//double - números decimais ex: 1.2535 / precisao de 15 / 16
		double precoDouble = 2.12345678911234567;
		System.out.println(precoDouble);
		
		//boolean - avlores booleanos - true / false
		boolean minhaVariavelBoolean = true;
		System.out.println(minhaVariavelBoolean);
	}
}
