package br.com.lucas;

public class EstruturasCondicional {

	public static void main(String[] args) {
		
		/*
		 if ( condicao ) {
		 	executa caso condicao seja satisfeita
		 }
		 */
		
		if(true) {
			System.out.println("Verdadeiro");
		} else if(false) {
			System.out.println("Verdadeiro ou falso");
		} else {
			System.out.println("falso");
		}
		
		int idade = 12;
		
		if(idade <= 12)
			System.out.println("Criança");
		else if(idade > 12 && idade <= 18)
			System.out.println("Adolescente");
		else if(idade > 18 && idade < 60)
			System.out.println("Adulto");
		else
			System.out.println("Idoso");

	}

}
