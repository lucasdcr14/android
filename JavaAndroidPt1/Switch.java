package br.com.lucas;

public class Switch {

    /*

        switch(valor) {
            case valorTestado:
                caso valor testado;
                break;
        }

        switch(1) {
            case(1):
                Valor 1;
                break;
            case(2):
                Valor 2;
                break;
            default:
                Valor padrão
        }

     */

    public static void main(String[] args) {

        switch (1) {
            case 1:
                System.out.println("Cartão de crédito selecionado");
                break;
            case 2: case 3:
                System.out.println("Saldão da sua conta");
                break;
            default:
                System.out.println("Falar com o atendente");
        }

        int nota = 8;

        switch (nota) {
            case 10: case 9:
                System.out.println("ótimo");
                System.out.println("Excelente aluno");
                break;
            case 8:
                System.out.println("bom");
                break;
            case 7:
                System.out.println("Regular");
                break;
            default:
                    System.out.println("Ruim");
        }

    }
}
