package br.com.lucas.classesemetodosnapratica;

public class Funcionario {

    //propriedades

    String nome;
    double salario;

    //métodos - sem retorno / com retorno
    /*void recuperarSalario() {
        this.salario = this.salario - (this.salario * 0.1);
        System.out.println(this.salario);
    }*/

    /*double recuperarSalario() {
        this.salario = this.salario - (this.salario * 0.1);
        //System.out.println(this.salario);
        return this.salario;
    }*/

    double recuperarSalario(double bonus, double descontoAdicional) {
        this.salario = this.salario - (this.salario * 0.1);
        //System.out.println(this.salario);
        return this.salario + bonus - descontoAdicional;
    }

}
