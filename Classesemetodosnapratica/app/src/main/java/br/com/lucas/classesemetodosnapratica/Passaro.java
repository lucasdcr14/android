package br.com.lucas.classesemetodosnapratica;

public class Passaro extends Animal {

    void voar() {
        System.out.println("Voar como um passáro");
    }

    void correr() {
        super.correr();
        System.out.println("cão");
    }
}
