package br.com.lucas.classesemetodosnapratica;

public class Animal {

    int tamanho;
    String cor;
    double peso;

    public int getTamanho() {
        return tamanho;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    //Getter e Setter
    void setCor(String cor) {
        //Formatação ou validação
        this.cor = cor;
    }

    public String getCor() {
        return cor;
    }

    void dormir() {
        System.out.println("Dormir como um ");
    }

    void correr() {
        System.out.println("Correr como um animal");
    }

}
