package br.com.lucas.classesemetodosnapratica;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //int numero = 10;
        //int numero;
        //numero = 0;

        /*Casa minhaCasa = new Casa();

        Casa minhaCasa2;
        minhaCasa2 = new Casa();

        minhaCasa.cor = "Azul";

        minhaCasa2.cor = "Branca";

        System.out.println(minhaCasa.cor);

        minhaCasa.abrirPorta();*/

        /*Funcionario funcionario = new Funcionario();
        funcionario.nome = "Lucas";
        funcionario.salario = 920;

        //funcionario.recuperarSalario();
        double salarioRecuperado = funcionario.recuperarSalario(150, 20);
        System.out.println("O salário é: " + (salarioRecuperado + 100));*/

        /*Animal animal = new Animal();
        animal.correr();*/

        /*Cao cao = new Cao();
        cao.dormir();
        cao.latir();

        cao.setCor("Azul");
        System.out.println(cao.getCor());

        Passaro passaro = new Passaro();
        passaro.correr();
        passaro.dormir();
        passaro.voar();*/

        Conta conta = new Conta();
        conta.depositar(100);
        conta.sacar(50);

        System.out.println(conta.recuperarSaldo());

    }
}
